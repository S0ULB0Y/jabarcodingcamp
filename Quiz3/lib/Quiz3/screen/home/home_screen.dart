import 'package:flutter/material.dart';
import 'package:myapp/Quiz3/screen/home/model.dart';

class Homescreen extends StatefulWidget {
  Homescreen({Key? key,required this.title}) : super(key: key);
  final String title;

  @override
  _HomescreenState createState() => _HomescreenState();
}

class _HomescreenState extends State<Homescreen> {
  int price=0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.only(top: 20),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(40.0),
                      child: Image.network(
                        "https://avatars.githubusercontent.com/u/52710807?v=4",
                        height: 80,
                        width: 80,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(widget.title.toLowerCase()),
                  ],
                ),
                Row(
                  children: [
                    //## soal 4
                    //Tulis Coding disini
                    Text("Rp $price "),

                    //sampai disini
                    SizedBox(
                      width: 10,
                    ),
                    Icon(Icons.add_shopping_cart)
                  ],
                ),
              ],
            ),
            SizedBox(height: 10),
            Container(
                height: MediaQuery.of(context).size.height /1.3,
                child: GridView.builder(
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    ),
                    itemCount: items.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Card(
                        color: Colors.black54,
                        shape: RoundedRectangleBorder(
                          borderRadius:BorderRadius.circular(10)
                        ),
                        child: Container(
                          margin:EdgeInsets.only(top:10,left:10,right:10,bottom:10),
                          decoration:BoxDecoration(
                            color: Colors.lightBlueAccent,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child:Column(
                            children: [
                              Container(
                                width:80,
                                height:80,
                                margin:EdgeInsets.only(top:0,left:0),
                                child:Image.network(items[index].profileUrl)
                              ),
                              Container(
                                margin:EdgeInsets.only(top:0,left:0,),
                                child:Text(items[index].name)
                              ),
                              Container(
                                margin:EdgeInsets.only(top:0,left:0),
                                child:Text(items[index].price.toString(),textAlign: TextAlign.center,)
                              ),
                              Flexible(
                                child:
                                Container(
                                  width:60,
                                  height:30,
                                  child:MaterialButton(color: Colors.blue,
                                    onPressed: (){
                                    setState(() {
                                      price+=items[index].price;
                                    });
                                  },
                                  child: Text("Beli")),
                              )),

                            ],
                          )
                        ),
                      );
                    })//silahkan dilanjutkan disini
            ),
            //#soal 3 silahkan buat coding disini
            //untuk container boleh di pake/dimodifikasi 


            //sampai disini soal 3
           
          ],
        ),
      ),
    );
  }
}

Widget myWidget(BuildContext context) {
  return MediaQuery.removePadding(
    context: context,
    removeTop: true,
    child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: items.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.amber,
            child: Center(child: Text('$index')),
          );
        }),
  );
}
