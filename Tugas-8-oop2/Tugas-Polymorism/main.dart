import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main() {
  lingkaran lingkaran1 = new lingkaran();
  persegi persegi1 = new persegi();
  segitiga segitiga1 = new segitiga();
  lingkaran1.hitung_luas_lingkaran(2.0);
  double hasil = lingkaran1.luas();
  print(hasil);
  print("Luas Lingkaran: " + lingkaran1.luas().toString());
  lingkaran1.hitung_keliling_lingkaran(2.0);
  print("Keliling Lingkaran: " + lingkaran1.keliling().toString());
  persegi1.hitung_luas_persegi(2.0);
  print("Luas Persegi: " + persegi1.luas().toString());
  persegi1.hitung_keliling_persegi(2.0);
  print("Keliling Persegi: " + persegi1.keliling().toString());
  segitiga1.hitung_luas_segitiga(2.0, 3.0);
  print("Luas Segitiga: " + segitiga1.luas().toString());
  segitiga1.hitung_keliling_segitiga(2.0, 3.0, 4.0);
  print("Keliling Segitiga " + segitiga1.keliling().toString());
}
