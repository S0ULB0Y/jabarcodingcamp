import 'bangun_datar.dart';

class lingkaran extends bangun_datar {
  double _phi = 3.14;
  double luaslingkaran = 0;
  double kelilinglingkaran = 0;

  void hitung_luas_lingkaran(double radius) {
    luaslingkaran = _phi * radius * radius;
  }

  @override
  double luas() {
    return luaslingkaran;
  }

  void hitung_keliling_lingkaran(double radius) {
    kelilinglingkaran = 2 * _phi * radius;
  }

  @override
  double keliling() {
    return kelilinglingkaran;
  }
}
