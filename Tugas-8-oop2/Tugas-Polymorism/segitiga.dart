import 'bangun_datar.dart';

class segitiga extends bangun_datar {
  late double luas_segitiga;
  late double keliling_segitiga;
  void hitung_luas_segitiga(double alas, double tinggi) {
    luas_segitiga = 0.5 * alas * tinggi;
  }

  void hitung_keliling_segitiga(double sisi1, double sisi2, double sisi3) {
    keliling_segitiga = sisi1 + sisi2 + sisi3;
  }

  @override
  double luas() {
    // TODO: implement luas
    return luas_segitiga;
  }

  @override
  double keliling() {
    // TODO: implement keliling
    return keliling_segitiga;
  }
}
