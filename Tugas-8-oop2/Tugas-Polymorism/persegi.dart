import 'bangun_datar.dart';

class persegi extends bangun_datar {
  double luas_persegi = 0;
  double keliling_persegi = 0;
  void hitung_luas_persegi(double sisi) {
    luas_persegi = sisi * sisi;
  }

  void hitung_keliling_persegi(double sisi) {
    keliling_persegi = 4 * sisi;
  }

  @override
  double luas() {
    // TODO: implement luas
    return luas_persegi;
  }

  @override
  double keliling() {
    // TODO: implement keliling
    return keliling_persegi;
  }
}
