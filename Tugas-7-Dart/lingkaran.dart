class Lingkaran {
  double _phi = 3.14;
  late double _radius;

  set radius(double value) {
    if (value < 0) {
      value * -1;
    }
    _radius = value;
  }

  double get radius {
    return _radius;
  }

  double get phi {
    return _phi;
  }

  double Luas_lingkaran() {
    return _phi * _radius * _radius;
  }
}
