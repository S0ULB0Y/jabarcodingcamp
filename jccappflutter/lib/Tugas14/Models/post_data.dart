
import 'package:flutter/material.dart';
import 'get_data.dart';
import "Get_data.dart";
import 'user_model.dart';
import 'package:http/http.dart' as http;

class PostDataApi extends StatefulWidget {
  PostDataApi({
    Key? key,
  }) : super(key: key);

  @override
  _PostDataApiState createState() => _PostDataApiState();
}

class Dialogs {
  static Future<void> showLoadingDialog(BuildContext context,
      GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,

              child: SimpleDialog(
// key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ), // SizedBox
                        Text(
                          "Please Wait..",
                          style: TextStyle(color: Colors.blueAccent),
                        ) // Text
                      ]), // Column
                    ) // center
                  ])); // <Widget>[] // simpleDialog // WillPopscope


        });
  }
}
class _PostDataApiState extends State<PostDataApi> {
  UserModel? _user;
  final TextEditingController titleController = TextEditingController();
  final TextEditingController valueController = TextEditingController();
  void _submitted(BuildContext context) async {
    final String title = titleController.text;
    final String value =valueController.text;
    final UserModel? user = await createUser(title, value);
    setState(() {
    _user = user;
    });
    Navigator.pop(context);
    }

    Future<UserModel?> createUser(
    String title,
    String value,
    ) async {
      try {
        Dialogs.showLoadingDialog(context, _keyLoader); //invoking login
        var apiUrl = Uri.parse(
            "https ://achmadhilmy-sanbercode.my.id/api/v1/news");
        final response = await http.post(
            apiUrl, body: {"title": title, "value": value});
        if (response.statusCode == 201) {
          final String responseString = response.body;
          return userModelFromJson(responseString);
        }
        Navigator.of(context).pop(context);
      } catch (e) {
        print(e.toString());
      }
    }
    final GlobalKey<State> _keyLoader= new GlobalKey<State>();
  @override
  Widget build(BuildContext context) {
  return Scaffold(
  appBar: AppBar(
  title: Text("Post Data"),
  ), // AppBar
  body: Container(
  padding: EdgeInsets.all(32),
  child: Column(
  children: <Widget>[
  TextField(
  controller: titleController,
  decoration: InputDecoration(
  border: OutlineInputBorder(), labelText: "title"), // InputDecoration
  ), // TextField
  SizedBox(height: 10),
  TextField(
  controller: valueController,
  decoration :  InputDecoration(
  border: OutlineInputBorder(), labelText: "Value"), // InputDecoration
  maxLines: 5,
  ), // TextField
  SizedBox(height: 10),
  _user==null
  ?Container()
      : Text(
  "the user ${_user!.title} is created, and id ${_user!.id}"), // Text
  ],

//Text("the user $(_user.name] is created, and id $(_user.id]"),
  )),

  floatingActionButton : FloatingActionButton(
  onPressed: () => _submitted(context),
  tooltip: 'Increment',
  child: Icon(Icons.add),
  ), // This trailing comma makes auto-formatting nicer for build methods. // FloatingActionButton
  );
  }
}

