import 'package:flutter/material.dart';
import 'package:jccappflutter/Tugas12/Drawer.dart';
/*
TODO:
How to set responsive and adaptive UI the widget doesnt resize  per widget

 */
class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key, required this.title}) : super(key: key);


  final String title;

  @override
  _HomeScreen createState() => _HomeScreen();
}

class _HomeScreen extends State<HomeScreen> {
  int _selectedIndex=0;
  @override
  Widget build(BuildContext context) {
    Size screensize=MediaQuery.of(context).size;
    final _widgetContent=<Widget>[
      Column(
          children:<Widget>[
            Container(
                margin:EdgeInsets.only(top:67.5,left:250),
                child:Row(
                    children:<Widget>[
                      Container(
                          child:Image(image:AssetImage("assets/icon/notifications.png"))
                      ),
                      Container(
                          margin:EdgeInsets.only(left:25),
                          child:Image(image:AssetImage("assets/icon/add_shopping_cart.png"))
                      )
                    ]
                )
            ),
            Container(
                alignment: Alignment.centerLeft,
                margin:EdgeInsets.only(left:42),
                child:Text("Welcome,",style: TextStyle(fontSize: 40,color:Colors.blue),)
            ),
            Container(
                alignment: Alignment.centerLeft,
                margin:EdgeInsets.only(left:42),
                child:Text("Dwika",style: TextStyle(fontSize: 40,color:Color(0xff01579B)),)
            ),
            Container(
                alignment: Alignment.centerLeft,
                margin:EdgeInsets.only(left:42,top:10,right:40),
                child:TextFormField(
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color:Colors.blue)
                    ),
                    prefixIcon:Icon(Icons.search),
                    hintText: "Search",

                  ),
                )
            ),

          Column(children: [
            Container(
                alignment: Alignment.centerLeft,
                margin:EdgeInsets.only(left:47,top:72),
                child:Text("Recomended Place",style: TextStyle(fontSize: 14),)
            ),
            Container(
                margin:EdgeInsets.only(top:15,left:49,right:50),
                child:Row(
                    children:<Widget>[
                      Container(
                          width:120,
                          height:70,
                          child:Image(image:AssetImage("assets/icon/Monas.png"))
                      ),
                      Container(
                          width:120,
                          height: 70,
                          margin:EdgeInsets.only(left:21),
                          child:Image(image:AssetImage("assets/icon/Roma.png"))
                      )
                    ]
                )
            ),
          Container(
                margin:EdgeInsets.only(top:14,left:49,right:50),
                child:Row(
                    children:<Widget>[
                      Container(
                          width:120,
                          height:70,
                          child:Image(image:AssetImage("assets/icon/Berlin.png"))
                      ),
                      Container(
                          width:120,
                          height: 70,
                          margin:EdgeInsets.only(left:21),
                          child:Image(image:AssetImage("assets/icon/Tokyo.png"))
                      )
                    ]
                )
            )
          ]
      )
          ]
      ),
      Text("Search"),
      Text("Account")

    ];
    return Scaffold(
        appBar: AppBar(title:Text("Home")),
        drawer:DrawerScreen(),
     body:_widgetContent.elementAt(_selectedIndex),
        resizeToAvoidBottomInset: false,
        bottomNavigationBar: BottomNavigationBar(items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label:"Home",


          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.search),
              label:"Search"
          ),
          BottomNavigationBarItem(
              icon:Icon(Icons.account_circle),
              label:"Account"
          )
        ],
          currentIndex:_selectedIndex,
          selectedItemColor: Colors.blue,
          onTap: (index){
    setState((){
      _selectedIndex=index;
    }
    );
    }
        )

    );
  }

}
