import 'package:flutter/material.dart';
import 'package:jccappflutter/Quiz3/HomeScreenQuiz.dart';
import 'HomeScreen.dart';
import 'package:cupertino_icons/cupertino_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:jccappflutter/Quiz3/HomeScreenQuiz.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key,this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String? title;

  @override
  _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
  final String password="jabarcodingcamp123";
  final usernameController= TextEditingController();
  final passwordController=TextEditingController();
  @override
  void dispose(){
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(left: 72, top: 49,right:73),
                child:Text("Sanber Flutter", style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color:Colors.blue))
            ),
            Container(
                margin:EdgeInsets.only(top:13),
                width: 93.8,
                height:100,
                child:Image(image: AssetImage('assets/icon/flutter.png'))
            ),
            Container(
              margin:EdgeInsets.only(left:47,top:29,right:35),
              child:TextFormField(
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(color:Colors.blue),
                      ),
                      hintText:"Username",

                  ),
                controller: usernameController,
              ),

            ),
            Container(
                margin: EdgeInsets.only(left:47,top: 29,right:35),
                child:TextFormField(
                    decoration: InputDecoration(
                      enabledBorder:OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide:BorderSide(color:
                        Colors.blue),
                      ),
                      hintText:"Password",
                    ),
                  controller: passwordController,
                  obscureText: true,
                )
            ),
            Container(
              margin: EdgeInsets.only(left:132,right:112,top:18.5),
              child:Text("Forgot Password",
                textAlign: TextAlign.center,
                style:TextStyle(fontSize: 14, color: Colors.blue),
              ),
            ),
            Container(
                width: 278,
                height: 42,
                margin: EdgeInsets.only(top:8,left:41,right:41),
                child:MaterialButton(
                  color:Colors.blue,
                  onPressed: () {
                    if(passwordController.text !=password){
                      final snackBar= SnackBar(content: const Text("Password yang anda masukan salah!"),
                        backgroundColor: Colors.red,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                    else {
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) =>
                             HomeScreenQuiz(title: usernameController.text)
                      ));
                    } },
                  child:Text("Login",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize:14,color:Colors.white)),
                )
            ),
            Container(
                margin:EdgeInsets.only(left:47,right:73,top:19),
                child: Row(
                  children: <Widget>[
                    Container(
                        margin:EdgeInsets.only(left:20),
                        child:Text("Does not have account?",
                            style: TextStyle(fontSize:14,color:Colors.black))
                    ),
                    Container(
                        margin:EdgeInsets.only(left:23),
                        child:Text("Sign in",
                            style:TextStyle(fontSize:14,color:Colors.blue))
                    ),
                  ],

                )
            ),
            Container(
                margin:EdgeInsets.only(top:66),
                child:Row(
                  children: <Widget>
                  [
                    Container(
                      width:120,
                      height:70,
                      margin:EdgeInsets.only(left:41),
                      decoration: BoxDecoration( color: Colors.blue),
                      child: Image(image:AssetImage("assets/icon/Eiffel.png")),

                    ),
                    Container(
                      width:120,
                      height:70,
                      margin:EdgeInsets.only(left:31),
                      decoration: BoxDecoration( color: Colors.blue),
                      child: Image(image:AssetImage("assets/icon/London.png"),
                        alignment: Alignment.center,),

                    ),
                  ],
                )
            )

          ],
        ),
    );
  }
}

