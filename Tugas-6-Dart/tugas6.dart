//No1
List<int> range(startNum, finishNum) {
  List<int> list = [];
  for (int i = startNum; i <= finishNum; i++) {
    list.add(i);
  }
  return list;
}

//No2
List<int> rangewithstep(startNum, finishNum, int step) {
  List<int> list = [];
  if (startNum < finishNum) {
    for (int i = startNum; i <= finishNum; i += step) {
      list.add(i);
    }
    return list;
  } else {
    for (int i = startNum; i >= finishNum; i -= step) {
      list.add(i);
    }
    return list;
  }
}
//No3

void data_handling(List input) {
  for (int i = 0; i < input.length; i++) {
    for (int j = 0; j < input[0].length; j++) {
      switch (j) {
        case 0:
          print("Nomor ID: " + input[i][j]);
          break;

        case 1:
          print("Nama Lengkap: " + input[i][j]);
          break;
        case 2:
          print("TTL: " + input[i][j]);
          break;
        case 3:
          print("Hobi: " + input[i][j]);
          break;
        default:
          break;
      }
    }
    print(" ");
  }
}

//No 4
String balikkata(String kata) {
  String word = "";

  for (int i = kata.length - 1; i >= 0; i--) {
    word += kata[i];
  }
  return word;
}

void main() {
  // var input = [
  //   ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  //   ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  //   ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  //   ["0004", "Bintang Senjaya", "Martapura", "6/4/1978", "Berkebun"]
  // ];
  // // print(range(11, 18));
  //print(rangewithstep(10,2,2))
  // data_handling(input);
  // print(balikkata("Kasur")); // rusak
  // print(balikkata("SanberCode")); // edocrebnas
  // print(balikkata("Haji")); // ijaH
  // print(balikkata("racecar")); // racecar
  // print(balikkata("Sanbers")); // srebnaS
}
