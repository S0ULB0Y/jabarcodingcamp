void main() {
  // var num1 = 12;
  // var num2 = 4;

  // var hasilKali = kalikan(num1, num2);
  // print(hasilKali); // 48
  // var name = "Agus";
  // var age = 30;
  // var address = "Jln. Malioboro, Yogyakarta";
  // var hobby = "Gaming";

  // var perkenalan = tampilnama(name, age, address, hobby);
  // print(
  //     perkenalan); // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta,
  //dan saya punya hobby yaitu Gaming!"
  // print(teriak()); // "Halo Sanbers!"
  print(faktorial(6));
}

//Tugas 1
// String teriak() {
//   return ("Halo Sanbers!");
// }
//Tugas 2
// int kalikan(int num1, int num2) {
//   return num1 * num2;
// }
//Tugas 3
// String tampilnama(String name, int age, String address, String hobby) {
//   return "Nama saya $name, umur saya $age , alamat saya di $address, dan saya punya hobby yaitu $hobby!";
// }
//Tugas 4 Faktorial
int faktorial(int number) {
  if (number == 0) {
    return 1;
  }
  return number * faktorial(number - 1);
}
