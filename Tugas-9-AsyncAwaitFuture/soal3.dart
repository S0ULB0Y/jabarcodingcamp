void main() {
  print("Ready. Sing");
  line();
  line2();
  line3();
  line4();
}

// line() {
//   print("pernahkan kau merasa");
// }

// line2() {
//   print("pernahkah kau meras...");
// }

// line3() {
//   print("pernahkah kau merasa");
// }

// line4() {
//   print("Hatimu hampa, pernahkah kau merasa hati mu kosong");
// }
Future<void> line() async {
  Future.delayed(
      Duration(seconds: 5), await () => print("pernahkan kau merasa..."));
}

Future<void> line2() async {
  Future.delayed(
      Duration(seconds: 8), await () => print("pernahkah kau merasa......."));
}

Future<void> line3() async {
  Future.delayed(
      Duration(seconds: 10), await () => print("pernahkah kau merasa"));
}

Future<void> line4() async {
  Future.delayed(
      Duration(seconds: 11),
      await () =>
          print("Hatimu hampa, pernahkah kau merasa hati mu kosong...."));
}
