import 'dart:io';

void main() {
  //desired output
//LOOPING PERTAMA
// 2 - I love coding
// 4 - I love coding
// 6 - I love coding
// 8 - I love coding
// 10 - I love coding
// 12 - I love coding
// 14 - I love coding
// 16 - I love coding
// 18 - I love coding
// 20 - I love coding
// LOOPING KEDUA
// 20 - I will become a mobile developer
// 18 - I will become a mobile developer
// 16 - I will become a mobile developer
// 14 - I will become a mobile developer
// 12 - I will become a mobile developer
// 10 - I will become a mobile developer
// 8 - I will become a mobile developer
// 6 - I will become a mobile developer
// 4 - I will become a mobile developer
// 2 - I will become a mobile developer

  // var i = 0;
  // while (i < 2) {
  //   var n = 2;
  //   print("LOOPING PERTAMA");
  //   while (n <= 20) {
  //     if (n == 20) {
  //       print("$n - I love coding");
  //       break;
  //     }
  //     print("$n - I love coding");
  //     n += 2;
  //   }

  //   i += 1;
  //   print("LOOPING KEDUA");
  //   while (n > 1) {
  //     print("$n - I will become a mobile developer");
  //     n -= 2;
  //   }
  //   ;
  //   i += 1;
  // }
  //Tugas 2
//   A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
  // for (int i = 1; i <= 20; i++) {
  //   if (i % 2 == 0) {
  //     print("$i - Berkualitas");
  //   } else if (i % 3 == 0 && i % 2 != 0) {
  //     print("$i - I love coding");
  //   } else {
  //     print("$i - Santai");
  //   }
  // }
  //Tugas 3
  // for (int i = 0; i < 4; i++) {
  //   for (int i = 0; i < 8; i++) {
  //     stdout.write("#");
  //   }
  //   print("");
  // }
  //Tugas 4
  // var k = 1;
  // for (int i = 0; i < 7; i++) {
  //   for (int i = 0; i < k; i++) {
  //     if (k <= 7) {
  //       stdout.write("#");
  //     }
  //   }
  //   k++;
  //   print("");
  // }
}
